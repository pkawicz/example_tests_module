#!/usr/bin/env python3

__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__version__ = '1.0'


def print_var(variable, times=1, **kwargs):
    print(str(variable)*times, **kwargs)


def ask_user(greetings="Hello: "):
    return input(greetings)


def read_file(filename):
    text = ""
    with open(filename, "r") as f:
        text += f.read()
    return text


def write_file(filename, text):
    with open(filename, "w+") as f:
        f.write(text)
